# [tim.zook.net](https://tim.zook.net)

This is a [Next.js](https://nextjs.org/) project for my personal website.

## Development

First, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
