import { NavMenu } from "@/components/NavMenu";
import { Container } from "@/components/Container";
import { FaLinkedin, FaGithub, FaGitlab } from "react-icons/fa";

export function Header() {
  return (
    <header className="sticky top-0 w-full bg-zinc-900/70 backdrop-blur-xl z-20 border-b border-[rgba(255,255,255,.1)]">
      <Container className="flex">
        <h1 className="font-header mr-4 my-5 flex items-center justify-center">
          <a href="#" className="drop-shadow-[0_0_3px_rgba(0,0,0,1)] text-4xl" title="aka zookatron">
            Tim Zook
          </a>
        </h1>
        <NavMenu className="grow">
          <NavMenu.Spacer />
          <NavMenu.Item href="#about-me">About Me</NavMenu.Item>
          <NavMenu.Item href="#projects">Projects</NavMenu.Item>
          <NavMenu.Item href="#skills">Skills</NavMenu.Item>
          <NavMenu.Item href="#contact">Contact</NavMenu.Item>
          <NavMenu.Spacer />
          <NavMenu.Item href="https://gitlab.com/zookatron" target="_blank" title="GitLab">
            <FaGitlab aria-label="GitLab" className="hidden md:block" />
            <span className="block md:hidden">GitLab</span>
          </NavMenu.Item>
          <NavMenu.Item href="https://github.com/zookatron" target="_blank" title="GitHub">
            <FaGithub aria-label="GitHub" className="hidden md:block" />
            <span className="block md:hidden">GitHub</span>
          </NavMenu.Item>
          <NavMenu.Item href="https://www.linkedin.com/in/timzook" target="_blank" title="LinkedIn">
            <FaLinkedin aria-label="LinkedIn" className="hidden md:block" />
            <span className="block md:hidden">LinkedIn</span>
          </NavMenu.Item>
        </NavMenu>
      </Container>
    </header>
  );
}
