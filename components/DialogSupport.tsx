"use client";

import { useEffect } from "react";

function handleDialogEvent(event: MouseEvent) {
  const target = event.target as HTMLElement;
  const anchor = target.nodeName === "A" ? (target as HTMLAnchorElement) : target.closest("a");
  const openDialog = anchor?.dataset.openDialog;
  if (openDialog) {
    const dialog = document.getElementById(openDialog) as HTMLDialogElement | null;
    if (dialog?.showModal) {
      dialog.showModal();
    }
  }
  const closeDialog = anchor?.dataset.closeDialog;
  if (closeDialog) {
    const dialog = document.getElementById(closeDialog) as HTMLDialogElement | null;
    if (dialog?.close) {
      dialog.close();
    }
  }
}

export function DialogSupport() {
  useEffect(() => {
    document.querySelector("body")?.classList.remove("nojs");
    document.addEventListener("click", handleDialogEvent);
    const initialTarget = document.getElementById(window.location.hash.replace("#", ""));
    if (initialTarget?.nodeName === "DIALOG") {
      (initialTarget as HTMLDialogElement).showModal();
    }
    return () => document.removeEventListener("click", handleDialogEvent);
  }, []);
  return <></>;
}
