import Image, { StaticImageData } from "next/image";
import { kebabCase } from "lodash-es";

export function AboutSection({
  title,
  image,
  imageClass,
  className,
  children,
}: {
  title: string;
  image: StaticImageData;
  imageClass?: string;
  className?: string;
  children: React.ReactNode;
}) {
  return (
    <div
      id={`about-section-${kebabCase(title)}`}
      className={`${className} scroll-mt-20 py-5 flex flex-col md:flex-row md:odd:flex-row-reverse items-start gap-5 relative`}
    >
      <Image src={image.src} alt="" width={image.width} height={image.height} className={`${imageClass} mx-auto`} />
      <div>
        <h3 className="font-header text-3xl pb-3 text-center md:text-left">{title}</h3>
        <div className="prose prose-invert max-w-none">{children}</div>
      </div>
    </div>
  );
}
