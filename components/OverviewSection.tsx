export function OverviewSection({
  title,
  className,
  children,
}: {
  title: string;
  className?: string;
  children: React.ReactNode;
}) {
  return (
    <div
      className={`${className} border border-zinc-700 bg-zinc-750 p-5 rounded-lg mb-6 flex flex-col items-center justify-center`}
    >
      <h3 className="text-2xl text-center font-semibold font-header pb-3.5">{title}</h3>
      <ul>{children}</ul>
    </div>
  );
}

OverviewSection.Item = function OverviewSectionItem({
  icon,
  link,
  className,
  children,
}: {
  icon: string;
  link: string;
  className?: string;
  children: React.ReactNode;
}) {
  return (
    <li className={`${className} pb-2.5 md:pb-1.5 last:pb-0`}>
      <a href={link} className="flex">
        <div className="pr-2">{icon}</div>
        <div>{children}</div>
      </a>
    </li>
  );
};
