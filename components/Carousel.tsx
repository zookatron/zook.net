import Image, { StaticImageData } from "next/image";
import { FaAngleLeft, FaAngleRight, FaDotCircle } from "react-icons/fa";

export function Carousel({
  id,
  images,
  className,
}: {
  id: string;
  images: Array<StaticImageData & { title: string }>;
  className?: string;
}) {
  return (
    <div className={`${className} flex relative`} aria-label="Carousel" aria-roledescription="carousel">
      <div className="pt-[75%] grow"></div>
      <ol
        id={`${id}-slides`}
        aria-live="polite"
        className="absolute top-0 bottom-0 left-0 right-0 overflow-y-hidden overflow-x-scroll scroll-smooth scrollbar-hide flex snap-x
        snap-mandatory snap-always"
      >
        {images.map((image, index) => (
          <li
            id={`${id}-slide-${index}`}
            key={index}
            tabIndex={0}
            aria-roledescription="slide"
            aria-label={`Slide ${index} of 6`}
            className="relative w-full shrink-0 snap-start"
          >
            <Image className="w-full" src={image} alt={image.title} width={image.width} height={image.height} />
            <a
              href={`#${id}-slide-${index === 0 ? images.length - 1 : index - 1}`}
              className="absolute top-1/2 -translate-y-1/2 left-2"
              aria-controls={`${id}-slides`}
              tabIndex={-1}
            >
              <FaAngleLeft aria-label="Go to previous slide" className="text-4xl drop-shadow-outline text-zinc-800" />
            </a>
            <a
              href={`#${id}-slide-${index === images.length - 1 ? 0 : index + 1}`}
              className="absolute top-1/2 -translate-y-1/2 right-2"
              aria-controls={`${id}-slides`}
              tabIndex={-1}
            >
              <FaAngleRight aria-label="Go to next slide" className="text-4xl drop-shadow-outline text-zinc-800" />
            </a>
          </li>
        ))}
      </ol>
      <div className="absolute bottom-3 left-0 right-0 flex items-center justify-center gap-3">
        {images.map((image, index) => (
          <a aria-controls={`${id}-slides`} tabIndex={-1} key={index} href={`#${id}-slide-${index}`}>
            <FaDotCircle aria-label={`Go to slide ${index}`} className="text-2xl drop-shadow-outline text-zinc-800" />
          </a>
        ))}
      </div>
    </div>
  );
}
