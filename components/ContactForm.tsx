"use client";

import { useState, useCallback } from "react";
import { Button } from "./Button";

async function sendToGoogleForms(name: string, email: string, message: string) {
  await fetch(
    "https://docs.google.com/forms/d/e/1FAIpQLSdvciPKIfqdXDRJ_sFaCzMKsUWuSq25Zg53ROh6ReLclCX3jQ/formResponse",
    {
      method: "POST",
      mode: "no-cors",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: new URLSearchParams({
        "entry.664369943": name,
        "entry.287763798": email,
        "entry.1912773308": message,
      }).toString(),
    }
  );
}

export function ContactForm() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [submitting, setSubmitting] = useState(false);
  const [result, setResult] = useState<boolean | null>(null);

  const updateName = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => setName(event.target.value),
    [setName]
  );
  const updateEmail = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => setEmail(event.target.value),
    [setEmail]
  );
  const updateMessage = useCallback(
    (event: React.ChangeEvent<HTMLTextAreaElement>) => setMessage(event.target.value),
    [setMessage]
  );
  const submit = useCallback(
    async (event: React.FormEvent<HTMLFormElement>) => {
      event.preventDefault();
      if (submitting) {
        return;
      }
      setResult(null);
      setSubmitting(true);
      try {
        await sendToGoogleForms(name, email, message);
        setName("");
        setEmail("");
        setMessage("");
        setResult(true);
      } catch (error) {
        console.error(error);
        setResult(false);
      }
      setSubmitting(false);
    },
    [name, email, message, submitting, setName, setEmail, setMessage, setSubmitting, setResult]
  );

  return (
    <form
      className="mb-10 p-5 bg-zinc-700 rounded-lg border border-zinc-600 max-w-[600px] m-auto flex flex-col items-center"
      onSubmit={submit}
    >
      <div className="grid grid-cols-form gap-4 mb-4 w-full">
        <div className="flex items-center justify-end">
          <label className="text-sm font-medium" htmlFor="contact-form-name">
            Name:
          </label>
        </div>
        <div className="flex">
          <div className="relative w-full">
            <input
              className="block w-full border disabled:cursor-not-allowed disabled:opacity-50 bg-zinc-600 border-zinc-500
              text-white focus:border-blue-500 focus:ring-blue-500 placeholder-zinc-400 rounded-lg p-2.5 text-sm"
              id="contact-form-name"
              type="text"
              placeholder="Your name"
              required
              value={name}
              onInput={updateName}
            />
          </div>
        </div>
        <div className="flex items-center justify-end">
          <label className="text-sm font-medium" htmlFor="contact-form-email">
            Email:
          </label>
        </div>
        <div className="flex">
          <div className="relative w-full">
            <input
              className="block w-full border disabled:cursor-not-allowed disabled:opacity-50 bg-zinc-600 border-zinc-500
              text-white focus:border-blue-500 focus:ring-blue-500 placeholder-zinc-400 rounded-lg p-2.5 text-sm"
              id="contact-form-email"
              type="email"
              placeholder="you@domain.com"
              required
              value={email}
              onInput={updateEmail}
            />
          </div>
        </div>
        <div className="flex items-start justify-end">
          <label className="mt-2 text-sm font-medium" htmlFor="contact-form-message">
            Message:
          </label>
        </div>
        <div className="flex">
          <div className="relative w-full">
            <textarea
              className="block w-full border disabled:cursor-not-allowed disabled:opacity-50 bg-zinc-600 border-zinc-500
              text-white focus:border-blue-500 focus:ring-blue-500 placeholder-zinc-400 rounded-lg p-2.5 text-sm"
              id="contact-form-message"
              placeholder="Hi there, my name is..."
              required
              value={message}
              onInput={updateMessage}
            />
          </div>
        </div>
      </div>
      {result === true ? (
        <div
          className="mb-4 border disabled:cursor-not-allowed disabled:opacity-50 border-green-400 bg-green-100
          text-green-900 placeholder-green-700 rounded-lg p-2.5 text-sm"
        >
          Thanks, message recieved!
        </div>
      ) : result === false ? (
        <div
          className="mb-4 border disabled:cursor-not-allowed disabled:opacity-50 border-red-400 bg-red-100
          text-red-900 placeholder-red-700 rounded-lg p-2.5 text-sm"
        >
          Sorry, something went wrong, please contact me on&nbsp;
          <a className="underline" href="https://www.linkedin.com/in/timzook" target="_blank">
            LinkedIn
          </a>
          &nbsp;to let me know!
        </div>
      ) : null}
      <Button type="submit" disabled={submitting} loading={submitting}>
        Send
      </Button>
    </form>
  );
}
