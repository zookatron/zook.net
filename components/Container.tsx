export function Container({ className, children }: { className?: string; children: React.ReactNode }) {
  return <div className={`container mx-auto px-5 xl:max-w-[1280px] ${className}`}>{children}</div>;
}
