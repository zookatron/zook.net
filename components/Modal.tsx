import { IoMdClose } from "react-icons/io";

export function Modal({ id, className, children }: { id: string; className?: string; children: React.ReactNode }) {
  return (
    <dialog
      id={id}
      role="dialog"
      aria-labelledby={`${id}-title`}
      aria-modal="true"
      className="bg-transparent text-zinc-100 z-30 nojs:block nojs:fixed nojs:top-0 group"
    >
      <div
        className="fixed top-0 bottom-0 left-0 right-0 overflow-x-hidden overflow-y-auto overscroll-contain scroll-smooth nojs:invisible
        nojs-group-target:visible nojs:opacity-0 nojs-group-target:opacity-100 nojs:transition-all nojs:duration-300 bg-zinc-900
        md:bg-zinc-900/80"
      >
        <div className="relative flex min-h-screen">
          <a
            href="#close"
            data-close-dialog={id}
            tabIndex={-1}
            className="absolute top-0 bottom-0 left-0 right-0 text-transparent cursor-pointer"
          >
            Close modal
          </a>
          <div
            className="fixed top-0 right-0 z-30 w-full md:w-auto flex justify-end bg-zinc-900/70 md:bg-transparent backdrop-blur-xl
            md:backdrop-blur-none z-20 border-b md:border-none border-[rgba(255,255,255,.1)]"
          >
            <a href="#close" data-close-dialog={id} className="p-2 md:p-5">
              <IoMdClose aria-label="Close modal" className="text-4xl" />
            </a>
          </div>
          <div className="flex flex-col grow">
            <div className="md:hidden w-full h-[52px]"></div>
            <div className={`${className} grow`}>{children}</div>
          </div>
        </div>
      </div>
    </dialog>
  );
}
