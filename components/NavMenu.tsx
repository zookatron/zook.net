import { Children, cloneElement } from "react";
import { FaBars } from "react-icons/fa";
import { ReactElement } from "react-markdown/lib/react-markdown";

export function NavMenu({ className, children }: { className?: string; children: React.ReactNode }) {
  return (
    <div id="nav-menu" className={`${className} group py-5 flex justify-end items-center`}>
      <a className="text-3xl flex group-target:hidden md:hidden" href="#nav-menu">
        <FaBars aria-label="Open menu" />
      </a>
      <a className="text-3xl hidden group-target:flex md:group-target:hidden" href="#close">
        <FaBars aria-label="Close menu" />
      </a>
      <ul className="grow items-center hidden md:flex">
        {Children.map(children, (child) => cloneElement(child as ReactElement))}
      </ul>
      <ul
        className="flex flex-col items-center md:hidden invisible group-target:visible opacity-0 group-target:opacity-100 scale-y-0
        group-target:scale-y-100 origin-top transition-all duration-300 overflow-y-scroll max-h-[calc(100vh-5rem-1px)] fixed left-0 right-0
        top-[calc(5rem+1px)] bg-zinc-950/80 backdrop-blur-xl border-b border-[rgba(255,255,255,.1)] py-2"
      >
        {Children.map(children, (child) => cloneElement(child as ReactElement))}
      </ul>
    </div>
  );
}

NavMenu.Item = function NavMenuItem({
  className,
  href,
  target,
  title,
  children,
}: {
  className?: string;
  href: string;
  target?: string;
  title?: string;
  children: React.ReactNode;
}) {
  return (
    <li className={`font-header text-xl md:max-lg:text-lg ${className}`}>
      <a
        className="px-4 py-2 flex drop-shadow-[0_0_3px_rgba(0,0,0,1)] hover:drop-shadow-[0_0_10px_rgba(255,255,255,1)] transition-all"
        href={href}
        target={target}
        title={title}
      >
        {children}
      </a>
    </li>
  );
};

NavMenu.Spacer = function NavMenuSpacer() {
  return <li className="grow" />;
};
