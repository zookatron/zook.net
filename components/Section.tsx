export function Section({
  style,
  className,
  id,
  children,
}: {
  style?: "primary" | "secondary";
  className?: string;
  id?: string;
  children: React.ReactNode;
}) {
  return (
    <section
      className={
        style === "secondary"
          ? `${className} flex scroll-mt-20 bg-zinc-800 border-y border-zinc-700`
          : `${className} flex scroll-mt-20 bg-network bg-center`
      }
      id={id}
    >
      {children}
    </section>
  );
}
