import Image from "next/image";
import loader from "@/public/images/loader.svg";

export function Button({
  style,
  className,
  href,
  type,
  disabled,
  loading,
  children,
}: {
  style?: "primary" | "secondary";
  className?: string;
  href?: string;
  type?: string;
  disabled?: boolean;
  loading?: boolean;
  children: React.ReactNode;
}) {
  return type === "submit" ? (
    <button type={type} disabled={disabled} className={`${className} ${classes.base} ${classes[style || "primary"]}`}>
      {loading ? <Image src={loader.src} alt="Loading" width={15} height={15} className="animate-spin mr-2" /> : null}
      {children}
    </button>
  ) : (
    <a href={href} className={`${className} ${classes.base} ${classes[style || "primary"]}`}>
      {children}
    </a>
  );
}

const classes = {
  base: `items-center appearance-none rounded font-header disabled:opacity-50 disabled:pointer-events-none box-border cursor-pointer
  inline-flex h-12 justify-center leading-none overflow-hidden relative text-left no-underline transition-all duration-0.15s
  select-none touch-manipulation whitespace-nowrap will-change-[box-shadow,transform] text-lg px-4 border-0 hover:-translate-y-0.5`,
  primary: `bg-zinc-200 text-zinc-800 shadow-[rgba(0,0,0,0.25)_0_2px_4px,rgba(0,0,0,0.3)_0_7px_13px_-3px,#71717a_0_-3px_0_inset]
  focus:shadow-[#71717a_0_0_0_1.5px_inset,rgba(0,0,0,0.25)_0_2px_4px,rgba(0,0,0,0.3)_0_7px_13px_-3px,#71717a_0_-3px_0_inset]
  hover:shadow-[rgba(0,0,0,0.25)_0_4px_8px,rgba(0,0,0,0.3)_0_7px_13px_-3px,#71717a_0_-3px_0_inset]
  active:shadow-[#71717a_0_3px_7px_inset] active:translate-y-0.5`,
  secondary: `bg-zinc-800 text-zinc-400 shadow-[rgba(0,0,0,0.25)_0_2px_4px,rgba(0,0,0,0.3)_0_7px_13px_-3px,#18181b_0_-3px_0_inset]
  focus:shadow-[#18181b_0_0_0_1.5px_inset,rgba(0,0,0,0.25)_0_2px_4px,rgba(0,0,0,0.3)_0_7px_13px_-3px,#18181b_0_-3px_0_inset]
  active:shadow-[#18181b_0_3px_7px_inset]`,
};
