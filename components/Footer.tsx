import { Container } from "@/components/Container";

export function Footer({ children }: { children: React.ReactNode }) {
  return (
    <footer className="bg-zinc-950 text-zinc-400">
      <Container className="py-10 text-center font-header">
        Copyright {new Date().getFullYear()} Tim Zook
        {children}
      </Container>
    </footer>
  );
}
