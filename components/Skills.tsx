"use client";

import { useState, useCallback } from "react";
import { FaSearch } from "react-icons/fa";
import { kebabCase } from "lodash-es";
import Image from "next/image";

interface Skill {
  name: string;
  level: string;
  image: string;
  description: string;
}

interface Project {
  title: string;
  skills: string[];
}

export function Skills({ skills, projects }: { skills: Skill[]; projects: Project[] }) {
  const [search, setSearch] = useState("");
  const [searchResults, setSearchResults] = useState(skills);

  const updateSearch = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setSearch(event.target.value);
      setSearchResults(skills.filter((skill) => skill.name.toLowerCase().includes(event.target.value.toLowerCase())));
    },
    [skills, setSearch, setSearchResults]
  );

  return (
    <div className="flex max-w-[800px] gap-5 mx-auto pb-10 flex-col sm:flex-row">
      <div className="sm:w-1/2 h-80 flex flex-col">
        <div className="relative mb-5 nojs:hidden">
          <div className="absolute inset-y-0 left-0 flex items-center pl-4 pointer-events-none">
            <FaSearch />
          </div>
          <input
            className="block w-full border disabled:cursor-not-allowed disabled:opacity-50 bg-zinc-600 border-zinc-500
            text-white focus:border-blue-500 focus:ring-blue-500 placeholder-zinc-400 rounded-lg p-2.5 text-sm pl-11"
            type="text"
            placeholder="Search"
            required
            value={search}
            onInput={updateSearch}
          />
        </div>
        <div className="flex flex-col border border-zinc-600 rounded-lg grow overflow-hidden">
          <ul className="flex flex-col overflow-x-hidden overflow-y-scroll pretty-scrollbar-light">
            {searchResults.map((skill) => (
              <li key={skill.name} className="bg-zinc-900 border-b border-zinc-700">
                <a href={`#skill-${kebabCase(skill.name)}`} className="block text-center p-2.5">
                  {skill.name}
                </a>
              </li>
            ))}
          </ul>
        </div>
      </div>
      <div className="sm:w-1/2 h-80 bg-zinc-700 rounded-lg border border-zinc-600 relative overflow-hidden">
        <div className="p-3 absolute top-0 left-0 right-0 bottom-0 flex justify-center items-center">
          <p className="text-lg italic text-zinc-300">Select an item from the list</p>
        </div>
        {skills.map((skill) => {
          const skillProjects = projects.filter((project) => project.skills?.includes(skill.name));
          return (
            <div
              key={skill.name}
              id={`skill-${kebabCase(skill.name)}`}
              className={`scroll-mt-32 pretty-scrollbar-dark sm:scroll-mt-60 bg-zinc-700 p-5 absolute top-0 left-0 right-0 bottom-0
                overflow-x-hidden overflow-y-auto hidden target:block z-10`}
            >
              <Image src={skill.image} alt={""} width={50} height={50} className="mx-auto mb-3" />
              <h3 className="text-lg text-center mb-2 font-bold font-header">{skill.name}</h3>
              <h3>
                <span className="text-zinc-400">Skill Level:</span> {skill.level}
              </h3>
              {!!skillProjects.length && (
                <h3 className="mb-3 mt-1">
                  <span className="text-zinc-400">Projects:</span>
                  {skillProjects.map((project) => (
                    <a
                      key={project.title}
                      className="ml-1 after:content-[','] last:after:content-['']"
                      href={`#project-${kebabCase(project.title)}`}
                      data-open-dialog={`project-${kebabCase(project.title)}`}
                    >
                      {project.title}
                    </a>
                  ))}
                </h3>
              )}
              <p className="mt-3">{skill.description}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
}
