import Image from "next/image";
import { kebabCase } from "lodash-es";
import { Modal } from "./Modal";
import { Carousel } from "./Carousel";

export function Project({
  title,
  subtitle,
  url,
  images,
  className,
  children,
}: {
  title: string;
  subtitle: string;
  url?: string;
  images: string[];
  className?: string;
  children: React.ReactNode;
}) {
  const id = `project-${kebabCase(title)}`;
  return (
    <div className={`${className} flex max-w-[413px] w-full md:w-1/2 xl:w-1/3 py-5 md:px-5`}>
      <a
        className="flex border border-[rgba(255,255,255,.1)] bg-zinc-800 rounded-lg overflow-hidden relative"
        href={`#${id}`}
        data-open-dialog={id}
      >
        <Image src={images[0]} alt={""} width={1200} height={900} />
        <div
          className={`absolute bottom-0 w-full bg-zinc-900/70 backdrop-blur-lg z-10 border-t border-[rgba(255,255,255,.1)]
          p-3 text-center`}
        >
          <h3 className="font-header text-2xl pb-2">{title}</h3>
          <h4 className="font-header">{subtitle}</h4>
        </div>
      </a>
      <Modal id={id} className="flex">
        <div className="pointer-events-none xl:max-w-[1280px] mx-auto md:my-20 md:gap-5 md:px-5 flex grow flex-col-reverse md:flex-row">
          <div
            className="pointer-events-auto p-5 md:w-1/2 bg-zinc-900/50 backdrop-blur-xl md:border border-[rgba(255,255,255,.1)]
            rounded-xl"
          >
            <div className="pb-5">
              <h2 className="text-3xl font-bold" id={`${id}-title`}>
                {title}
              </h2>
              {!!url && (
                <a href={url} target="_blank" className="underline">
                  {url}
                </a>
              )}
            </div>
            <div className="prose prose-invert">{children}</div>
          </div>
          <div className="md:w-1/2">
            <Carousel
              id={`${id}-carousel`}
              className="pointer-events-auto overflow-hidden md:border border-[rgba(255,255,255,.1)] md:rounded-xl md:fixed w-full
              md:w-[calc((100vw-3.75rem)/2)] md:max-w-[610px] md:top-20"
              images={images.map((image, index) => ({
                src: image,
                title: `${title} #${index + 1}`,
                width: 1200,
                height: 900,
              }))}
            />
          </div>
        </div>
      </Modal>
    </div>
  );
}
