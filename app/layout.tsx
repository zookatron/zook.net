import "./globals.css";
import type { Metadata } from "next";
import { Poppins } from "next/font/google";

const headerFont = Poppins({ weight: "400", subsets: ["latin"], variable: "--font-header", display: "swap" });

export const metadata: Metadata = {
  title: "Tim Zook",
  description:
    "Tim Zook is an independent creative thinker with over a decade of professional web development experience.",
  referrer: "origin-when-cross-origin",
  authors: [{ name: "Tim Zook", url: "https://tim.zook.net" }],
  colorScheme: "dark",
  metadataBase: new URL("https://tim.zook.net"),
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en" className="scroll-smooth">
      <body className={`nojs ${headerFont.variable} bg-zinc-900 text-zinc-100`}>{children}</body>
    </html>
  );
}
