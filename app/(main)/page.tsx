import Image from "next/image";
import fs from "fs";
import path from "path";
import matter from "gray-matter";
import ReactMarkdown from "react-markdown";
import { Container } from "@/components/Container";
import { Section } from "@/components/Section";
import { Button } from "@/components/Button";
import { OverviewSection } from "@/components/OverviewSection";
import { AboutSection } from "@/components/AboutSection";
import { ContactForm } from "@/components/ContactForm";
import { Project } from "@/components/Project";
import { Footer } from "@/components/Footer";
import { Skills } from "@/components/Skills";
import portrait from "@/public/images/portrait.webp";
import quality from "@/public/images/quality.svg";
import planning from "@/public/images/planning.svg";
import testing from "@/public/images/testing.svg";
import culture from "@/public/images/culture.svg";
import collaborate from "@/public/images/collaborate.svg";

export const metadata = {
  alternates: {
    canonical: "/",
  },
};

function getData(folder: string) {
  const projectsDir = path.resolve(process.cwd(), folder);
  return fs
    .readdirSync(projectsDir)
    .sort()
    .map((filename) => {
      const projectFile = path.join(projectsDir, filename);
      const fileContents = fs.readFileSync(projectFile, "utf8");
      return matter(fileContents);
    });
}

export default function Home() {
  return (
    <>
      <Section>
        <Container className="flex py-10 md:py-20 flex-col-reverse md:flex-row">
          <div className="md:w-1/2 grow flex flex-col justify-center items-center">
            <h2 className="font-header text-5xl text-center">Hi, I&apos;m Tim!</h2>
            <p className="py-10 text-center text-xl max-w-[600px]">
              I&apos;ve been passionate about programming since I was old enough to type, and I&apos;ve been getting
              paid to do it for over a decade. I love creating robust, effective, and elegant solutions with web
              technologies.
            </p>
            <div className="flex gap-5">
              <Button href="#contact">Let&apos;s chat</Button>
              <Button style="secondary" href="/resume.pdf">
                Download CV
              </Button>
            </div>
          </div>
          <div className="pb-8 md:pb-0 mx-auto max-w-md md:max-w-none md:w-1/2 grow px-[10%] flex justify-center items-center">
            <Image
              src={portrait.src}
              alt="Portrait of Tim Zook"
              className="rounded-full shadow-hero mb-4"
              width={portrait.width}
              height={portrait.height}
            />
          </div>
        </Container>
      </Section>
      <Section style="secondary" id="about-me">
        <Container className="pb-10">
          <h2 className="font-header text-5xl text-center py-10">About Me</h2>
          <p className="text-xl text-center pb-10 max-w-[1000px] mx-auto">
            I&apos;m an independent creative thinker with over a decade of experience developing software using web
            technologies. I am passionate about helping companies design and implement effective technical solutions by
            writing high-quality code that both arrives on time and stands the test of time.
          </p>
          <div className="flex flex-col items-center justify-center">
            <OverviewSection title="Who am I?">
              <OverviewSection.Item icon="💼" link="#projects">
                I have extensive experience with a wide range of web development technologies and with technical
                leadership.
              </OverviewSection.Item>
              <OverviewSection.Item icon="🔥" link="#about-section-what-im-looking-for">
                I&apos;m passionate about innovating and proactively moving my projects forward, not just doing the bare
                minimum.
              </OverviewSection.Item>
              <OverviewSection.Item icon="📚" link="#about-section-step-1-careful-planning">
                I&apos;m a lifelong autodidact. I&apos;m pepetually curious, and I learn new technologies and processes
                very quickly.
              </OverviewSection.Item>
              <OverviewSection.Item icon="🔍" link="#about-section-step-2-robust-quality-assurance">
                I&apos;m highly conscientious, detail-oriented, and deeply insightful. I turn chaos into order.
              </OverviewSection.Item>
            </OverviewSection>
            <OverviewSection title="Who are you?">
              <OverviewSection.Item icon="💻" link="#about-section-what-im-looking-for">
                I&apos;m looking for senior web development positions (principal engineer, full-stack, frontend,
                backend, DevOps, etc)
              </OverviewSection.Item>
              <OverviewSection.Item icon="👥" link="#about-section-step-3-encouraging-learning-and-improvement">
                I&apos;m looking for companies that foster clear and open communication and encourage growth.
              </OverviewSection.Item>
              <OverviewSection.Item icon="🏆" link="#about-section-my-philosophy-code-quality-faster-velocity">
                I&apos;m looking for companies that share my passion for software craftsmanship.
              </OverviewSection.Item>
              <OverviewSection.Item icon="🗓️" link="#about-section-what-im-looking-for">
                I&apos;m looking for companies that have flexibility on non-standard work schedules.
              </OverviewSection.Item>
            </OverviewSection>
          </div>
          <AboutSection
            title="My philosophy: Code quality = Faster velocity"
            image={quality}
            imageClass="max-w-[40%] md:max-w-[30%]"
          >
            <p>
              In the modern age of software development where moving fast is everything, some people make the mistake of
              thinking this means we must sacrifice code quality for the sake of development speed. I firmly believe the
              opposite, that the only way to achieve faster implementation and iteration times is by relentlessly
              focusing on increasing code quality. Sacrificing code quality quickly leads to incomprehensible systems
              where it is impossible to change anything without breaking ten other things, which slows progress down to
              a crawl. The only way to produce a system that is flexible enough to enable rapid development is to ensure
              your application is properly structured and documented to provide clarity for developers regarding its
              operation. At the end of the day, there is no true trade-off to be had between quality and quantity in
              software development. The only way to consistently generate high quantities of code is through a sustained
              focus on code quality. My approach to ensuring high code quality incorporates a combination of careful
              planning to avoid mistakes before they happen, robust quality assurance to fix mistakes as quickly as
              possible when they arise, and encouraging a culture that seeks to constantly learn and improve from
              mistakes.
            </p>
          </AboutSection>
          <AboutSection title="Step 1: Careful Planning" image={planning} imageClass="max-w-[40%] md:max-w-[30%]">
            <p>
              Employing agile software development processes doesn&apos;t mean that you can just jump straight into
              coding without thinking things through. Proper planning is essential for ensuring that development work
              stays continually focused on the overall project goals throughout the rapid iteration cycles that agile
              software development enables. Planning does not necessarily mean endlessly agonizing over every last
              detail, but it does require developing a comprehensive strategy that incorporates a clear picture of the
              project priorities and potential risks before you start writing any code. It is vital to take into account
              many different priorities when designing an effective software system, such as financial priorities,
              feature priorities, end-user priorities, software administrator priorities, developer priorities,
              management priorities, etc. Any project that is implemented without taking all of these important
              priorities into account is likely to fail. Particularly when the proper approach is uncertain and further
              experimentation is required, it is essential to ensure that the proper framework is in place to allow that
              experimentation to proceed as quickly and efficiently as possible.
            </p>
            <p>
              I have many years of experience planning out extensive software development projects from start to finish
              using agile software development methodologies. My extensive experience has given me not only the
              technical knowledge of how to execute a plan, but also the judgment to be able to determine what ought to
              be done in the first place. However, I believe that an even more important contributor to my successful
              planning than my experience has been my strong ability to learn new technologies very quickly. The only
              constant in any growing business is change, and I thrive in these dynamic environments as I love learning
              and deepening my understanding of new technologies that can provide better solutions to upcoming problems.
              I have an extensive track record of learning complex new technologies enough to effectively apply them
              within a few weeks and becoming an expert that can easily guide others in the details of best practices
              within a few months.
            </p>
          </AboutSection>
          <AboutSection
            title="Step 2: Robust Quality Assurance"
            image={testing}
            imageClass="max-w-[48%] md:max-w-[35%]"
          >
            <p>
              Writing high-quality code is a form of communication, and like all communication, it is a mix of art and
              science. In order to be able to reliably create code that properly instructs the computer to do the right
              thing, the code must first be readable and understandable by the humans that determine what the right
              thing to do is. This requires following a coding practice that prioritizes factors like good organization,
              succinctness, and consistency, as well as technical factors like security, performance, and correctness.
              All quality assurance practices should be carefully designed to serve this core goal of clear and
              effective communication to foster shared understanding and collective success.
            </p>
            <p>
              I subscribe to a software craftsmanship philosophy that prioritizes not just coding quickly but also
              thinking deeply about the ultimate purpose of your code. I always take the time to be sure I fully
              understand the nature of the problem that I&apos;m working on before trying to solve it to ensure that I
              use the time spent implementing the solution as effectively as possible, and I try to help others learn
              this skill as well. During my years of experience leading a development team, I have had the opportunity
              to employ a wide range of quality assurance practices, such as code reviews, pair programming, automated
              testing, continuous integration, end-to-end monitoring, etc. While all of these tools help improve code
              quality, ultimately I&apos;ve found that no procedure can fully replace the need for genuine care on the
              part of the developer for creating well-crafted solutions.
            </p>
          </AboutSection>
          <AboutSection
            title="Step 3: Encouraging Learning and Improvement"
            image={culture}
            imageClass="max-w-[40%] md:max-w-[30%]"
          >
            <p>
              In order to create a development culture that consistently generates high-quality code, the entire team
              needs to be aligned on pursuing this goal. I believe that the most important aspect of this is developing
              a culture of trust where people feel they are able to give open and honest feedback to anyone on the team.
              Promoting healthy communication is essential for this, on both the giving and the receiving end. Employees
              need to be trained in giving feedback that is compassionate and constructive, as well as trained to accept
              feedback with humility and positivity in the spirit of continual improvement. Only through clear and open
              communication can the unique strengths and weaknesses of each team member be leveraged to the maximum
              extent to generate success.
            </p>
            <p>
              I have been leading a development team for over four years and have grown immensely through the
              experience, and I seek to help everyone around me to grow in turn. I have had the privilege of getting to
              help many new developers grow their technical and soft skills, as well as getting to learn from many other
              developers smarter than me. In addition to internal communication skills, I have also gained extensive
              experience with communicating directly with customers to collect feedback, learn more about their
              priorities, manage expectations, and generally ensure an excellent customer service experience. I deeply
              value all the lessons I&apos;ve learned from interacting with the real people that use the software I
              create, and it brings me great joy to see the value that my work has added to their lives.
            </p>
          </AboutSection>
          <AboutSection title={"What I'm looking for:"} image={collaborate} imageClass="max-w-[48%] md:max-w-[35%]">
            <p>
              I am interested in working with companies that share my passion for creating high-quality software.
              Specifically, I am looking for senior individual contributor positions in web-based industries, especially
              with some component of leadership like team lead or principal engineer positions. I want to be getting my
              hands dirty with writing code on a daily basis, but also to have the ability to actively influence the
              direction of the projects I&apos;m working on. I have zero interest in doing the bare minimum needed to
              collect a paycheck, when I work on a project I want to be actively engaged in looking for opportunities to
              improve its chances of success and watching out for pitfalls that could lead to failure. I also have a
              soft spot for open-source technologies because I love the freedom and empowerment that they give to the
              software development community, I would love to work on open-source technologies full-time.
            </p>
            <p>
              I highly prioritize working a 4-day day work week. I&apos;m looking for savvy companies that are
              interested in a unique opportunity to acquire a highly productive employee at a discount in exchange for
              flexibility on work schedules. I find that my overall output is highest when I work this schedule, and I
              think it does not make sense for people to pay me more for me to do less. I am grateful that I have had
              the opportunity to work this schedule for most of my career, and I look forward to finding more companies
              that are open to the advantages that this trade-off enables.
            </p>
            <p>
              If you want to learn more about me as a person outside my career, I would love to connect with you and
              chat more sometime, feel free to <a href="#contact">drop me a line</a>!
            </p>
          </AboutSection>
        </Container>
      </Section>
      <Section id="projects">
        <Container>
          <h2 className="font-header text-5xl text-center py-10">Past Projects</h2>
          <p className="text-xl text-center max-w-[1000px] mx-auto">
            At the end of the day, talk is cheap and results are what really matter. I have an extensive history of
            successful projects under my belt, here are just a few of the highlights from my career:
          </p>
          <p className="text-lg italic text-zinc-400 text-center pb-5">(Click on a project to learn more)</p>
          <div className="pb-10 flex flex-wrap justify-around">
            {getData("projects").map((project) => (
              <Project
                key={project.data.title}
                title={project.data.title}
                subtitle={project.data.subtitle}
                url={project.data.url}
                images={project.data.images}
              >
                <ReactMarkdown>{project.content}</ReactMarkdown>
              </Project>
            ))}
          </div>
        </Container>
      </Section>
      <Section style="secondary" id="skills">
        <Container>
          <h2 className="font-header text-5xl text-center pt-10 pb-8">Skills</h2>
          <p className="text-xl text-center pb-8 max-w-[1000px] mx-auto italic text-zinc-400">
            Buzzwords! Get your buzzwords here!
          </p>
          <Skills
            skills={getData("skills").map((skill) => ({
              name: skill.data.name,
              level: skill.data.level,
              image: skill.data.image,
              description: skill.content,
            }))}
            projects={getData("projects").map((project) => ({
              title: project.data.title,
              skills: project.data.skills,
            }))}
          />
        </Container>
      </Section>
      <Section id="contact">
        <Container>
          <h2 className="font-header text-5xl text-center py-10">Let&apos;s chat</h2>
          <p className="text-xl text-center pb-10 max-w-[1000px] mx-auto">
            I&apos;d love to meet you and discover how we can help each other out, please send me a message below!
          </p>
          <ContactForm />
        </Container>
      </Section>
      <Footer>
        <span className="px-2">|</span>
        Images by vectorjuice and macrovector on Freepik:
        <span className="pl-1 inline-flex gap-1">
          {images.map((image, index) => (
            <a key={index} href={image} target="_blank">
              {index + 1}
            </a>
          ))}
        </span>
      </Footer>
    </>
  );
}

const images = [
  "https://www.freepik.com/free-vector/focus-group-business-research-data-analytics-company-profitable-strategy-" +
    "planning-dartboard-computer-monitor-corporate-goals-achievements-concept-illustration_11667288.htm",
  "https://www.freepik.com/free-vector/real-estate-development-abstract-concept_12085291.htm",
  "https://www.freepik.com/free-vector/search-engine-marketing-business-copywriting-service-content-management_12085329.htm",
  "https://www.freepik.com/free-vector/corporate-meeting-employees-cartoon-characters-discussing-business-" +
    "strategy-planning-further-actions-brainstorming-formal-communication-seminar-concept-illustration_11668427.htm",
  "https://www.freepik.com/free-vector/businessmen-shaking-hands-through-laptop-screens-as-online-business-conference-meeting_13450332.htm",
  "https://www.freepik.com/free-vector/management-icons-set_4411455.htm",
  "https://www.freepik.com/free-vector/business-training-consulting-service-icons-set_3789869.htm",
  "https://www.freepik.com/free-vector/programmer-icons-flat-set_4411453.htm",
  "https://www.freepik.com/free-vector/cloud-computing-flat-icons-set_3975529.htm",
];
