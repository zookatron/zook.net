import { Header } from "@/components/Header";
import { DialogSupport } from "@/components/DialogSupport";

export default function MainLayout({ children }: { children: React.ReactNode }) {
  return (
    <>
      <Header />
      <main>{children}</main>
      <DialogSupport />
    </>
  );
}
