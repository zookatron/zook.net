import Image from "next/image";
import portrait from "@/public/images/portrait.webp";

export const metadata = {
  alternates: {
    canonical: "/card",
  },
};

export default function Card() {
  return (
    <div
      className={`bg-zinc-300 flex min-h-screen items-stretch lg:items-center justify-stretch lg:justify-center
      [print-color-adjust:exact] text-xs font-[Arial]`}
    >
      <div
        className={`lg:my-10 lg:max-w-[7in] lg:shadow-[0_0_5rem_0_#000] h-[4in] overflow-hidden grow bg-zinc-800
        flex flex-col relative`}
      >
        <div className="flex grow">
          <div className="flex w-2/5 bg-network bg-[length:300px] border-r border-zinc-700 p-8 justify-center items-center">
            <Image
              src={portrait.src}
              alt="Portrait of Tim Zook"
              className="rounded-full shadow-hero mb-4"
              width={portrait.width}
              height={portrait.height}
            />
          </div>
          <div className="flex flex-col w-3/5 px-8 py-16">
            <h1 className="font-header text-6xl mb-3">Tim Zook</h1>
            <h2 className="font-header text-[18.5px]">Senior Full Stack Developer @ Dispel</h2>
            <div className="flex-grow"></div>
            <h3 className="font-header text-2xl mb-3">investing@zook.net</h3>
            <h3 className="font-header text-2xl">682-325-9665</h3>
          </div>
        </div>
      </div>
    </div>
  );
}
