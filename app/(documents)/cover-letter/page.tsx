import Image from "next/image";
import portrait from "@/public/images/portrait.webp";
import qrcode from "@/public/images/qrcode.png";
import { FaLinkedin, FaGitlab, FaEnvelope, FaMapMarkedAlt, FaGlobe } from "react-icons/fa";

export const metadata = {
  alternates: {
    canonical: "/cover-letter",
  },
};

export default function Resume() {
  return (
    <div
      className={`bg-zinc-300 flex min-h-screen items-stretch lg:items-center justify-stretch lg:justify-center
      [print-color-adjust:exact] text-xs font-[Arial]`}
    >
      <div
        className={`lg:my-10 lg:max-w-[8.5in] lg:shadow-[0_0_5rem_0_#000] h-[11in] overflow-hidden grow bg-zinc-800
        flex flex-col relative`}
      >
        <div className="flex justify-around bg-zinc-950 border-b border-zinc-700">
          <a href="https://tim.zook.net" className="flex items-center p-3 whitespace-nowrap">
            <FaGlobe className="mr-2" />
            tim.zook.net
          </a>
          <a href="mailto:contact@zook.net" className="flex items-center p-3 whitespace-nowrap">
            <FaEnvelope className="mr-2" />
            contact@zook.net
          </a>
          <a href="https://www.openstreetmap.org/relation/113314" className="flex items-center p-3 whitespace-nowrap">
            <FaMapMarkedAlt className="mr-2" />
            Austin, TX
          </a>
          <a href="https://www.linkedin.com/in/timzook" className="flex items-center p-3 whitespace-nowrap">
            <FaLinkedin className="mr-2" />
            linkedin.com/in/timzook
          </a>
          <a href="https://gitlab.com/zookatron" className="flex items-center p-3 whitespace-nowrap">
            <FaGitlab className="mr-2" />
            gitlab.com/zookatron
          </a>
        </div>
        <div className="absolute z-10 top-[5.5rem] left-0 right-0 flex bg-zinc-950 border-y border-zinc-700">
          <Image
            src={portrait.src}
            alt="Portrait of Tim Zook"
            className="absolute top-1/2 left-[calc(100%/6)] -translate-y-1/2 -translate-x-1/2 border border-zinc-700 max-w-[20%]"
            width={portrait.width}
            height={portrait.height}
          />
          <div className="w-1/3 relative"></div>
          <div className="w-2/3 flex flex-col justify-center px-7 py-5">
            <h1 className="font-header text-5xl mb-2">Tim Zook</h1>
            <h2 className="font-header text-2xl">Senior Web Developer</h2>
          </div>
        </div>
        <div className="flex grow">
          <div className="w-1/3 bg-network bg-[length:300px] border-r border-zinc-700 p-8 pt-52 relative">
            <p className="text-2xl italic pt-4 text-zinc-400">
              Thank you for taking the time to read this, I look forward to hearing from you soon!
            </p>
            <div className="pt-[150px]">
              <h3 className="text-lg flex items-center mb-2 font-bold font-header">References</h3>
              <p className="text-sm mb-3">
                <span className="font-bold">Ricardo Casas</span>
                <br />
                CEO at Fahrenheit Marketing
                <br />
                <a href="mailto:rcasas@fahrenheitmarketing.com">rcasas@fahrenheitmarketing.com</a>
                <br />
                <a href={`tel:${formatPhoneNumber(142789 * 35926)}`}>{formatPhoneNumber(142789 * 35926)}</a>
              </p>
              <p className="text-sm mb-3">
                <span className="font-bold">Brandon Dunham</span>
                <br />
                COO at Fahrenheit Marketing
                <br />
                <a href="mailto:bdunham@fahrenheitmarketing.com">bdunham@fahrenheitmarketing.com</a>
                <br />
                <a href={`tel:${formatPhoneNumber(5 * 1025976109)}`}>{formatPhoneNumber(5 * 1025976109)}</a>
              </p>
              <p className="text-sm mb-3">
                <span className="font-bold">Thomas Umstattd</span>
                <br />
                CEO at Author Media
                <br />
                <a href="mailto:thomas@authormedia.com">thomas@authormedia.com</a>
                <br />
                <a href={`tel:${formatPhoneNumber(3121 * 1641664)}`}>{formatPhoneNumber(3121 * 1641664)}</a>
              </p>
            </div>
            <Image
              src={qrcode.src}
              alt="Portrait of Tim Zook"
              className="absolute bottom-14 right-1/2 translate-x-1/2 w-1/3"
              width={qrcode.width}
              height={qrcode.height}
            />
          </div>
          <div className="w-2/3 p-8 pt-48 prose prose-invert max-w-none text-base">
            <p>
              Hi there! I&apos;m an independent creative thinker with over a decade of professional web development
              experience. I think I have a combination of skills and drive that would make a big difference at a place
              like &lt;company name&gt;. Here are just a few of the reasons why I&apos;m interested in the opportunity
              to work together:
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sit amet orci bibendum, aliquam nibh
              eu, ultricies dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed posuere justo in dui
              pharetra, nec commodo nunc sollicitudin. Donec et aliquam magna. Curabitur consectetur libero at ipsum
              tincidunt, non faucibus orci condimentum. Orci varius natoque penatibus et magnis dis parturient montes,
              nascetur ridiculus mus. Sed rhoncus porttitor ultrices. Aenean tempus varius erat ac bibendum. Curabitur
              sagittis nulla diam, at semper leo elementum sodales. Nam eleifend velit blandit diam vulputate efficitur
              quis at nisl. Nam efficitur neque quis felis pharetra porta.
            </p>
            <p>
              Nam vel venenatis sapien. Nunc et hendrerit justo, id tristique mi. Nunc ac posuere neque. Sed sit amet
              magna ac purus maximus lacinia. Nam id mi id leo elementum porta et sed dui. Donec eleifend sagittis nunc,
              vel mollis odio rutrum quis. Ut pulvinar massa nec sapien ultrices cursus. Aliquam dapibus ut magna sit
              amet volutpat. Cras vitae arcu a nibh laoreet suscipit.
            </p>
            <p>
              Cras non iaculis erat, vel viverra nisl. Suspendisse potenti. Aliquam dictum vulputate maximus. Sed orci
              lectus, aliquam sit amet porta ut, sodales sed ex. Nulla condimentum felis eu dictum dapibus. Phasellus
              sodales, metus sit amet congue laoreet, quam velit venenatis sapien, a congue enim felis et tortor.
              Maecenas tincidunt et velit euismod congue.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

function formatPhoneNumber(input: number) {
  const inputString = `${input}`;
  return `${inputString.slice(0, 3)}-${inputString.slice(3, 6)}-${inputString.slice(6, 10)}`;
}
