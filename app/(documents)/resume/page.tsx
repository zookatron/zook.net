import Image from "next/image";
import portrait from "@/public/images/portrait.webp";
import qrcode from "@/public/images/qrcode.png";
import {
  FaLinkedin,
  FaGitlab,
  FaEnvelope,
  FaMapMarkedAlt,
  FaGlobe,
  FaBook,
  FaUser,
  FaBriefcase,
  FaSchool,
} from "react-icons/fa";
import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { kebabCase } from "lodash-es";

export const metadata = {
  alternates: {
    canonical: "/resume",
  },
};

function getData(folder: string) {
  const projectsDir = path.resolve(process.cwd(), folder);
  return fs
    .readdirSync(projectsDir)
    .sort()
    .map((filename) => {
      const projectFile = path.join(projectsDir, filename);
      const fileContents = fs.readFileSync(projectFile, "utf8");
      return matter(fileContents);
    });
}

export default function Resume() {
  return (
    <div
      className={`bg-zinc-300 flex min-h-screen items-stretch lg:items-center justify-stretch lg:justify-center
      [print-color-adjust:exact] text-xs font-[Arial]`}
    >
      <div
        className={`lg:my-10 lg:max-w-[8.5in] lg:shadow-[0_0_5rem_0_#000] h-[11in] overflow-hidden grow bg-zinc-800
        flex flex-col relative`}
      >
        <div className="flex justify-around bg-zinc-950 border-b border-zinc-700">
          <a href="https://tim.zook.net" className="flex items-center p-3 whitespace-nowrap">
            <FaGlobe className="mr-2" />
            tim.zook.net
          </a>
          <a href="mailto:contact@zook.net" className="flex items-center p-3 whitespace-nowrap">
            <FaEnvelope className="mr-2" />
            contact@zook.net
          </a>
          <a href="https://www.openstreetmap.org/relation/113314" className="flex items-center p-3 whitespace-nowrap">
            <FaMapMarkedAlt className="mr-2" />
            Austin, TX
          </a>
          <a href="https://www.linkedin.com/in/timzook" className="flex items-center p-3 whitespace-nowrap">
            <FaLinkedin className="mr-2" />
            linkedin.com/in/timzook
          </a>
          <a href="https://gitlab.com/zookatron" className="flex items-center p-3 whitespace-nowrap">
            <FaGitlab className="mr-2" />
            gitlab.com/zookatron
          </a>
        </div>
        <div className="absolute top-[5.5rem] left-0 right-0 flex bg-zinc-950 border-y border-zinc-700">
          <Image
            src={portrait.src}
            alt="Portrait of Tim Zook"
            className="absolute top-1/2 left-[calc(100%/6)] -translate-y-1/2 -translate-x-1/2 border border-zinc-700 max-w-[20%]"
            width={portrait.width}
            height={portrait.height}
          />
          <div className="w-1/3 relative"></div>
          <div className="w-2/3 flex flex-col justify-center px-7 py-5">
            <h1 className="font-header text-5xl mb-2">Tim Zook</h1>
            <h2 className="font-header text-2xl">Senior Web Developer</h2>
          </div>
        </div>
        <div className="flex grow">
          <div className="w-1/3 bg-network bg-[length:300px] border-r border-zinc-700 p-8 pt-52">
            <Section icon={<FaBook className="mr-2" />} title="Skills" className="mt-2">
              <ul className="text-sm list-dot list-inside">
                {getData("skills").map((skill) => (
                  <li key={skill.data.name}>
                    <a href={`https://tim.zook.net/#skill-${kebabCase(skill.data.name)}`} className="underline">
                      {skill.data.name}
                    </a>
                  </li>
                ))}
              </ul>
            </Section>
            <Section icon={<FaSchool className="mr-2" />} title="Education">
              <h4 className="text-base">Texas A&M University</h4>
              <div>Computer Science - 4.0 GPA</div>
            </Section>
            <Image
              src={qrcode.src}
              alt="Portrait of Tim Zook"
              className="mt-7 mx-auto w-1/3"
              width={qrcode.width}
              height={qrcode.height}
            />
          </div>
          <div className="w-2/3 p-8 pt-48">
            <Section className="mt-1.5" icon={<FaUser className="mr-2" />} title="About Me">
              <p>
                I&apos;m an independent creative thinker with over a decade of experience developing software using web
                technologies. I&apos;ve been the CTO of a successful web development agency for four years, during which
                time I have architected, managed, and developed dozens of complex web applications and integrations. I
                am looking for senior individual contributor positions in web-based industries, especially with some
                component of leadership like team lead or principal engineer positions. You can learn more about my
                software development philosophy and career goals on{" "}
                <a href="https://tim.zook.net" className="underline pointer font-bold">
                  my website
                </a>
                .
              </p>
              <p className="mt-2">
                I highly prioritize working a 4-day day work week. I&apos;m looking for savvy companies that are
                interested in a unique opportunity to acquire a highly productive employee at a discount in exchange for
                flexibility on work schedules.
              </p>
            </Section>
            <div className="relative">
              <div className="absolute top-3 left-[6px] w-1 rounded-full bottom-0 bg-zinc-400" />
              <Section icon={<FaBriefcase className="mr-2 bg-zinc-800" />} title="Experience" className="z-10 relative">
                <Section
                  icon={<div className="w-4 h-4 bg-zinc-300 rounded-full mr-3 border-2 border-zinc-800" />}
                  title="Jun 2019 - May 2023: CTO at Fahrenheit Marketing"
                  titleClass="text-sm font-normal"
                >
                  <p className="pl-7">
                    When our previous CTO left the company I stepped up to fill the leadership vacuum, taking
                    responsibility for ensuring our ongoing projects were completed successfully to earn myself the
                    title of Chief Technical Officer. As CTO I continued to excel in a technical leadership position,
                    revolutionizing our DevOps processes with projects like{" "}
                    <a
                      href="https://tim.zook.net/#project-fahrenheit-thermostat"
                      className="underline pointer font-bold"
                    >
                      Thermostat
                    </a>{" "}
                    and securing many important new clients like{" "}
                    <a
                      href="http://tim.zook.net/#project-pepsi-co-resource-library"
                      className="underline pointer font-bold"
                    >
                      PepsiCo
                    </a>{" "}
                    and{" "}
                    <a href="http://tim.zook.net/#project-pre-will" className="underline pointer font-bold">
                      PreWill
                    </a>
                    . I wore many different hats in this role, doing server administration to ensure all our client
                    websites ran reliably, doing project management to coordinate with clients and keep our developers
                    on track, and doing full-stack development to ensure our most challenging projects were implemented
                    flawlessly.
                  </p>
                </Section>
                <Section
                  icon={<div className="w-4 h-4 bg-zinc-300 rounded-full mr-3 border-2 border-zinc-800" />}
                  title="Jul 2016 - Jun 2019: Web Developer at Fahrenheit Marketing"
                  titleClass="text-sm font-normal"
                >
                  <p className="pl-7">
                    Joining Fahrenheit Marketing greatly expanded the repertoire of technologies that I was proficient
                    with, I learned many new tools like Laravel, Docker, and React during my early years. I also learned
                    many important communication skills and principles of agile software development while working here.
                    As a core member of the development team, I made significant contributions to many different
                    projects such as{" "}
                    <a href="http://tim.zook.net/#project-firmspace" className="underline pointer font-bold">
                      Firmspace
                    </a>{" "}
                    and{" "}
                    <a href="http://tim.zook.net/#project-lammes-candies" className="underline pointer font-bold">
                      Lammes Candies
                    </a>
                    .
                  </p>
                </Section>
                <Section
                  icon={<div className="w-4 h-4 bg-zinc-300 rounded-full mr-3 border-2 border-zinc-800" />}
                  title="Aug 2012 - Jul 2016: Freelance Web Developer"
                  titleClass="text-sm font-normal"
                >
                  <p className="pl-7">
                    As a freelance web developer, I worked with a variety of clients to independently create robust,
                    responsive WordPress themes from mockups. I also continued consulting for Castle Media Group to
                    develop and maintain several interactive WordPress plugins like{" "}
                    <a href="https://wordpress.org/plugins/mybooktable/" className="underline pointer font-bold">
                      MyBookTable
                    </a>{" "}
                    and{" "}
                    <a href="https://wordpress.org/plugins/mybookprogress/" className="underline pointer font-bold">
                      MyBookProgress
                    </a>
                    .
                  </p>
                </Section>
                <Section
                  icon={<div className="w-4 h-4 bg-zinc-300 rounded-full mr-3 border-2 border-zinc-800" />}
                  title="Aug 2011 - Aug 2012: Castle Media Group - Web Developer"
                  titleClass="text-sm font-normal"
                >
                  <p className="pl-7">
                    At Castle Media Group I taught myself PHP, WordPress, and a variety of other web technologies and
                    swiftly became a top-performing member of the development team. We created custom WordPress websites
                    and web applications primarily targeted at the book author submarket.
                  </p>
                </Section>
              </Section>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function Section({
  className,
  titleClass,
  children,
  icon,
  title,
}: {
  className?: string;
  titleClass?: string;
  children: React.ReactNode;
  icon: React.ReactNode;
  title: string;
}) {
  return (
    <div className={`${className} mb-4`}>
      <h3 className={`${titleClass} text-base flex items-center mb-2 font-bold font-header`}>
        {icon}
        {title}
      </h3>
      {children}
    </div>
  );
}
