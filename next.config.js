/** @type {import('next').NextConfig} */

export default {
  experimental: {
    esmExternals: true,
    appDir: true,
  },
  output: "export",
  images: { unoptimized: true },
};
