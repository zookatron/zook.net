import puppeteer from "puppeteer";
import { createServer } from "http-server";
import { fileURLToPath } from "url";
import path from "path";
import fs from "fs";

async function main() {
  const dirname = path.dirname(fileURLToPath(import.meta.url));
  const server = createServer({ root: path.resolve(dirname, "out") });
  server.listen(8080);
  const browser = await puppeteer.launch({ headless: "new", args: ["--no-sandbox"] });
  const page = await browser.newPage();
  await page.goto("http://127.0.0.1:8080/resume", { waitUntil: "networkidle0" });
  const pdf = await page.pdf({ format: "Letter" });
  await browser.close();
  await fs.promises.writeFile(path.resolve(dirname, "public/resume.pdf"), pdf);
  server.close();
}

main();
