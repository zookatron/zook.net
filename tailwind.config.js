/** @type {import('tailwindcss').Config} */
const { fontFamily } = require("tailwindcss/defaultTheme");
const plugin = require("tailwindcss/plugin");

module.exports = {
  content: ["./components/**/*.{ts,tsx}", "./app/**/*.{ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        header: ["var(--font-header)", ...fontFamily.sans],
      },
      backgroundImage: {
        network: "url('/images/network.svg')",
      },
      boxShadow: {
        hero: "5px 10px 0 -1px rgba(63, 63, 70, 0.9)",
      },
      gridTemplateColumns: {
        form: "fit-content(100px) 1fr",
      },
      dropShadow: {
        outline: [
          "1px 1px 0 rgb(244 244 245 / 1)",
          "1px -1px 0 rgb(244 244 245 / 1)",
          "-1px -1px 0 rgb(244 244 245 / 1)",
          "-1px 1px 0 rgb(244 244 245 / 1)",
        ],
      },
      listStyleType: {
        dot: '"\u2022 "',
      },
      colors: {
        zinc: {
          750: "#2C2C31",
        },
      },
    },
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("tailwind-scrollbar-hide"),
    plugin(({ addVariant }) => {
      addVariant("nojs", "body.nojs &");
      addVariant("nojs-group-target", "body.nojs .group:target &");
    }),
    plugin(({ addComponents }) => {
      addComponents({
        ".pretty-scrollbar-light": {
          scrollbarColor: "#52525b #18181b",
          "&::-webkit-scrollbar": {
            backgroundColor: "#18181b",
            width: "10px",
          },
          "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#52525b",
            border: "2px solid #18181b",
            borderRadius: "5px",
          },
        },
        ".pretty-scrollbar-dark": {
          scrollbarColor: "#27272a #52525b",
          "&::-webkit-scrollbar": {
            backgroundColor: "#52525b",
            width: "10px",
          },
          "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#27272a",
            border: "2px solid #52525b",
            borderRadius: "5px",
          },
        },
      });
    }),
  ],
};
