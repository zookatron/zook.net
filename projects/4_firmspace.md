---
title: Firmspace
subtitle: Node.js web application
url: https://firmspace.com/
skills:
  - Non-violent Communication
  - Technical Writing
  - Full-stack Web Development
  - Linux System Administration
  - Software Architecture
  - Distributed System Design
  - Cloud Deployment Models
  - Software Quality Assurance
  - Progressive Web Apps
  - REST API Design
  - TypeScript/JavaScript
  - Node.js/Deno
images:
  - /images/projects/firmspace1.webp
  - /images/projects/firmspace2.webp
  - /images/projects/firmspace3.webp
---

Firmspace is a high-end coworking business that came to my company with an ambitious web application idea. They wanted to create a private social media platform for their clientele to help them meet one another, communicate about events happening in their building, and schedule group room bookings. I played a key role on the team that designed and implemented this vision using a serverless architecture powered by AWS Lambda and DynamoDB.

### Communication & Planning

I worked closely with the lead developer on this project to clarify and refine the specifications for the project and to review the project outlines in detail to ensure they were complete. Together we were able to formulate and execute a formidable implementation plan that incorporated over 100 API endpoints and over 30 unique page designs.

### Learning & Expertise

The Firmspace project was my company's first time using most of the technologies that it employed, including AWS Lambda, DynamoDB, Angular, and TypeScript. I quickly became our company's leading expert in all of these technologies, and I regularly helped my team members improve their understanding of the tools to help them excel. The knowledge I gained working on this project served as a solid basis for my future work, and we have continued to look for new opportunities to branch out with new technologies ever since.

### Technical Excellence

This project played a formative role in establishing my understanding of how important well-structured code is to increasing development velocity and reliability. We faced many challenges on this project due to poorly organized and convoluted code, and I played a key role in helping my team reorganize and refactor this code to help improve reliability and speed further development. For example, when we faced challenges with slow page loading times due to long HTTP request dependency chains, I independently designed and implemented a custom type-safe query generator and resolver framework that allowed us to dynamically combine multiple API calls into a single HTTP request. This framework let us simultaneously simplify the code, improve its performance, and resolve numerous bugs.
