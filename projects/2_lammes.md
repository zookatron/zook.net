---
title: Lammes Candies
subtitle: Custom BigCommerce store extensions
url: https://www.lammes.com/
skills:
  - Engineering Leadership
  - Non-violent Communication
  - Project Management
  - Technical Writing
  - Full-stack Web Development
  - Linux System Administration
  - Software Architecture
  - Continuous Integration
  - Software Quality Assurance
  - Progressive Web Apps
  - SQL DB Administration
  - REST API Design
  - GitOps
  - PHP/Composer
  - TypeScript/JavaScript
  - React/Vue.js
  - Tailwind CSS/Bootstrap
  - Laravel
  - BigCommerce/Shopify
  - Docker
images:
  - /images/projects/lammes1.webp
  - /images/projects/lammes2.webp
  - /images/projects/lammes3.webp
---

Lammes Candies came to my company in dire straits, with their Oracle-based eCommerce solution that they had recently migrated to failing on them spectacularly during their busy holiday season. I architected and helped implement an entirely new eCommerce solution for them based on BigCommerce, a popular SAAS eCommerce platform. My team created customized React-based product, cart, and checkout interfaces for their new store that supported several custom functions like future order scheduling, streamlined multi-recipient ordering, and in-store pickup fulfillment. We also created a Laravel application that integrated with the BigCommerce API to provide a fully custom ordering interface to improve their call center efficiency, custom order management tools to facilitate their fulfillment processes, bulk order processing functionality to streamline the creation of large orders, and custom financial reporting systems to integrate with their ERP.

### Leadership & Planning

I worked closely with Lammes to collect and refine the requirements of their new eCommerce functionality, produce estimates on the work required and set realistic expectations, and provide support for the ongoing maintenance of their store. I helped manage the development team that implemented the work to ensure we delivered the project on time and according to the agreed-upon specifications. My clear and prompt communication played a key role in turning Lammes into one of my company's biggest fans.

### Learning & Expertise

Lammes was my company's first major BigCommerce project. During the planning and implementation of this project, I was able to quickly learn and integrate the BigCommerce API into a comprehensive eCommerce solution. I quickly became our company's leading expert in BigCommerce, able to effectively answer client questions about the platform, put together documentation on reliable best practices, and coach other developers on how to use the platform effectively. I ended up learning the BigCommerce API better than some of BigCommerce's own engineers and I helped them improve their documentation and fix several bugs in their API.

### Technical Excellence

I designed a robust and scalable architecture to meet Lammes' business needs, utilizing tools like Elasticsearch for fast real-time searches and a custom SMTP server for automating the integration with an antiquated email-based system they relied on. I also played a major role in turning the design into an efficient, well-organized, and easily maintainable codebase; Approximately 75% of the code Lammes uses in production today was written exclusively by me. The system I helped build for them has allowed them to grow their business significantly and has served them well for nearly five years with zero outages caused by defects in our systems.
