---
title: Fahrenheit Thermostat
subtitle: Docker-based deployment system
url: https://thermostat.fahrenheit.io/docs/
skills:
  - Engineering Leadership
  - Non-violent Communication
  - Technical Writing
  - Linux System Administration
  - Software Architecture
  - Distributed System Design
  - Cloud Deployment Models
  - Continuous Integration
  - Software Quality Assurance
  - SQL DB Administration
  - REST API Design
  - GitOps
  - TypeScript/JavaScript
  - Docker
  - Node.js/Deno
images:
  - /images/projects/thermostat1.webp
  - /images/projects/thermostat2.webp
  - /images/projects/thermostat3.webp
---

When I joined Fahrenheit Marketing, we were plagued with chronic inefficiencies in our deployment environment creation and maintenance processes. Our developers would typically work on several different client projects each day, and they frequently had to spend many hours of their time (and other developers' time helping them) just getting their basic tools and dependencies configured to be able to start working on a new project. We would also constantly run into issues with failures in production caused by outdated dependencies and configuration oversights. I independently designed and developed the Fahrenheit Thermostat tool that completely solved these issues for us and gave us a significant competitive advantage due to the efficiency and reliability of our systems. Thermostat is a Docker-based CLI tool that provides reliable standardized solutions for dependency management, development process automation, automated deployments, backup management, and integrations with many other common development tools.

### Leadership & Strategy

Before I took a leadership role at Fahrenheit Marketing, there was a concerning leadership vacuum around development processes and metrics. I was able to independently identify several key problems with our processes and effectively demonstrate the impact these issues were having on our business. We saw immediate benefits in development velocity and deployment reliability after the deployment of Fahrenheit Thermostat, and we have continued investing in improving our processes ever since.

### Creativity & Innovation

Once I had identified the core issues that were holding our development team back, I was able to put together a creative and practical solution that effectively utilized technologies that were completely new to use at the time, such as Docker. Thermostat has an innovative architecture that utilizes Docker not only for containerizing standard web software like web servers and databases, but also for development utilities like testing and database administration tools, running standardized tasks and scripts, and even running the Thermostat CLI logic itself. Using Docker has dramatically simplified the process of developers getting up to speed on our projects and has made it easy to implement reliable and well-documented deployment processes. These processes have consistently saved us time and money and improved the experience that our clients have with us. Thermostat also utilizes SSH tunnels for handling authentication and encrypting the communication of confidential client data between servers, providing an easy and intuitive user experience for our developers while maintaining the highest possible level of security.

### Technical Excellence

I independently developed the entire Fahrenheit Thermostat application, which incorporates a Node.js-based API server, a flexible cross-platform CLI, and a React-based web interface prototype. I was able to finish prototyping, testing, and deploying the initial version in less than two months, and we have continued iterating on and improving the system ever since. In addition to the application implementation, I also created a robust collection of documentation for the system that has allowed other developers to use and modify the tool effectively. The Thermostat deployment system has proved remarkably reliable for us, with zero outages occurring due to defects in the tooling in the four years it has been integrated with our 100+ projects.
