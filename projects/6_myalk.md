---
title: MyALK
subtitle: Laravel web application
url: https://myalkus.com/
skills:
  - Engineering Leadership
  - Non-violent Communication
  - Project Management
  - Technical Writing
  - Full-stack Web Development
  - Linux System Administration
  - Software Architecture
  - Continuous Integration
  - Software Quality Assurance
  - SQL DB Administration
  - REST API Design
  - GitOps
  - PHP/Composer
  - TypeScript/JavaScript
  - React/Vue.js
  - Tailwind CSS/Bootstrap
  - Laravel
images:
  - /images/projects/myalk1.webp
  - /images/projects/myalk2.webp
  - /images/projects/myalk3.webp
---

ALK is an allergy testing business that came to my company for help with an aging customer purchasing application that had numerous performance and maintainability issues. Trying to keep it running smoothly and fix its bugs was costing them time and money and blocking them from adding the features they wanted. I helped them create a new custom web application using Laravel & React with a beautiful new design and an expanded and improved feature set for purchasing, loyalty reward management, customer information management, and importing and exporting data to and from their ERP.

### Planning & Strategy

I worked closely with ALK to identify pain points with their previous solution and to help brainstorm the new implementation so we could be sure all their priorities were accounted for properly. The new application was extensive in scope and they knew they needed a team that wouldn't miss any of the important details. I independently created a detailed set of implementation plans and estimates on the work required, which helped give the customer the confidence they needed to move forward with the project knowing they were in good hands.

### Leadership & Expertise

Armed with a well-thought-out implementation plan, I lead the development team that brought the application to life. In addition to contributing directly to the development, I also personally reviewed every line of code added to the system to ensure it met our company standards for organization, readability, security, and performance. I helped mentor some of the developers working on the project, working with them to help them grow their skills and self-confidence.

### Technical Excellence

The application I designed and helped implement for ALK has proved to be a highly profitable and dependable part of their business, allowing them to quickly and effectively manage their customer purchases. The careful organization and clean coding principles that I ensured were instilled into the application have given us the flexibility to easily maintain the deployment and add new features to the software over time, such as a new product visibility system for limiting certain types of product purchases to specific customer groups. The new architecture has maintained high performance as MyALK has added thousands of new customers, and we have had zero outages or security incidents related to the application implementation in the three years it's been in service.
