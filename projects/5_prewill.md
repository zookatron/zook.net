---
title: PreWill
subtitle: Capacitor mobile application
url: https://app.prewill.com/
skills:
  - Engineering Leadership
  - Non-violent Communication
  - Project Management
  - Technical Writing
  - Full-stack Web Development
  - Linux System Administration
  - Software Architecture
  - Continuous Integration
  - Software Quality Assurance
  - Progressive Web Apps
  - SQL DB Administration
  - REST API Design
  - GitOps
  - PHP/Composer
  - TypeScript/JavaScript
  - React/Vue.js
  - Tailwind CSS/Bootstrap
  - Laravel
  - Docker
images:
  - /images/projects/prewill1.webp
  - /images/projects/prewill2.webp
  - /images/projects/prewill3.webp
---

PreWill is a startup that partnered with my company to bring their new vision to life: A mobile application that would enable people to send personalized final messages to loved ones in the event of an unexpected passing. I architected and helped implement a combined web and mobile application for them using Laravel and Capacitor. The application we built provided a streamlined user signup and account management experience, secure and reliable activity tracking to detect unexpected passings, and an easy-to-use message management interface that supported a broad range of platforms.

### Leadership & Planning

I worked closely with PreWill to brainstorm a creative and cost-effective technical solution for their use case that balanced technical capabilities, user priorities, and financial priorities. I put together a detailed set of implementation plans and estimates on the work required for several approaches to the solution and worked with the client to determine which one best fit their needs. Once we found a solid plan of attack, I lead the development team that implemented the application and ensured my team's ability to execute the plan uncompromisingly. Even after the implementation itself was complete, PreWill faced many challenges with the Apple and Google Play app approval process that I helped them navigate successfully with clear and effective communication.

### Learning & Expertise

PreWill was my company's first major mobile app development project. Due to this, our successful execution depended heavily on our ability to utilize many new tools like Capacitor, the Google Play APIs, and the iOS platform APIs. The activity tracking component of the application proved particularly challenging as the mobile platforms had recently taken action to try to limit applications from being able to track users in general which was intended to shield users from invasive advertising systems. Even though our app did not have any advertising component, these changes required us to thoroughly learn the details of these platform APIs so that we could devise new and creative ways to push the systems to their limits to ensure a reliable and efficient tracking implementation. I was able to effectively master these APIs over the course of a few weeks and played a major role in implementing the most complex components of the application myself.

### Technical Excellence

Implementing this project required me to not only write clean and efficient application code, but also to develop new and effective processes for facilitating the development and testing of the application. I implemented a comprehensive suite of development tools for the project that allowed us to quickly test our mobile app changes locally to speed up development and facilitate coordination between the developers on the implementation work. I also set up a robust continuous integration solution to automate the validation and deployment process of updating the mobile apps on the app stores. Using these tools my team and I implemented a rock-solid and highly scalable set of new APIs and user interfaces that has served as a reliable foundation for PreWill's new business.
