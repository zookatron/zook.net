---
title: PepsiCo Resource Library
subtitle: Custom WordPress functionality
url: https://resources.pepsicorecycling.com/
skills:
  - Engineering Leadership
  - Non-violent Communication
  - Project Management
  - Technical Writing
  - Full-stack Web Development
  - Linux System Administration
  - Software Architecture
  - Distributed System Design
  - Cloud Deployment Models
  - Continuous Integration
  - Software Quality Assurance
  - SQL DB Administration
  - REST API Design
  - GitOps
  - PHP/Composer
  - TypeScript/JavaScript
  - React/Vue.js
  - WordPress
  - Docker
images:
  - /images/projects/pepsico1.webp
  - /images/projects/pepsico2.webp
  - /images/projects/pepsico3.webp
---

The PepsiCo Recycling team partnered with my company to rework the aging resource library that they used to provide materials to parents and teachers to help promote recycling. Their existing system was frustrating to use both from an end-user perspective and an administration perspective due to its confusing page layouts and convoluted category hierarchies. I architected and helped implement a new WordPress website for them that solved these issues and added a wealth of new features to enable visitors to find resources more effectively.

### Leadership & Planning

I worked closely with the PepsiCo Recycling team to identify pain points with their previous solution and to help them clarify their goals for the new implementation. Based on these discussions, I put together a comprehensive set of specifications and implementation plans to guide the development of their new website. Armed with this detailed plan, I also lead the development team that implemented the solution and helped guide the project to success.

### Creativity & Expertise

Due to the application's central focus on content management, we knew we wanted to use the WordPress CRM framework to power our solution. However, significant custom functionality had to be added to the platform to meet the project's core goals. I developed a custom set of interface extensions for the WordPress admin panel and a highly optimized React-based search interface that made both managing the resources on the back end and finding them on the front end easy, fast, and scalable.

### Technical Excellence

One of PepsiCo's primary requirements for the new website was a guarantee of stability, performance, and security to ensure the solution could reliably operate at the scale of a large multinational corporation. To satisfy this, I designed and implemented a high-availability server architecture utilizing multiple redundant load-balanced application servers, a robust database cluster, and an S3-compatible file storage system to ensure there was no single point of failure in the entire setup. PepsiCo has enjoyed zero unplanned downtime on the website since its deployment two years ago, other than a single large-scale networking incident that affected the entire region where their website was deployed.
