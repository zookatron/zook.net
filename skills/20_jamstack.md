---
name: JAMstack
level: Experienced
image: /images/skills/jamstack.svg
---

I have extensive professional experience developing modern web applications using powerful JAMstack technologies such as NextJS, GatsbyJS, Gridsome, Contentful, etc.
