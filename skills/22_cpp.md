---
name: C/C++
level: Hobbyist
image: /images/skills/cpp.svg
---

I have significant personal experience with developing C and C++ desktop applications. I have a strong understanding of manual memory management systems.
