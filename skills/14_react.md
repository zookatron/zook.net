---
name: React/Vue.js
level: Expert
image: /images/skills/react.svg
---

I have been developing and maintaining complex React applications on a daily basis for over 7 years. I have worked on several extensive applications in other frameworks like Vue.js and Angular, but I prefer React.
