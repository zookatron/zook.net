---
name: Docker
level: Expert
image: /images/skills/docker.svg
---

As CTO of Fahrenheit Marketing, I helped revolutionize our company's development processes by implementing, deploying, and maintaining extensive Docker-based deployment tooling. I am a big fan of Docker in general, I do 100% of my web development work in Docker environments nowadays.
