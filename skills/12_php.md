---
name: PHP/Composer
level: Expert
image: /images/skills/php.svg
---

I have been developing and maintaining complex PHP web applications on a daily basis for over a decade.
