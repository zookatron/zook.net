---
name: Software Quality Assurance
level: Expert
image: /images/skills/quality_assurance.svg
---

I'm highly passionate about creating high-quality software. As CTO of Fahrenheit Marketing, I closely managed our code review processes, automated testing processes, and other quality assurance systems for four years.
