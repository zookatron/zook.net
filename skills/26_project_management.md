---
name: Project Management
level: Experienced
image: /images/skills/project_management.svg
---

As CTO of Fahrenheit Marketing, I worked closely with clients to plan and coordinate ambitious projects for four years. I managed a team of 5-10 developers to bring those ideas to life.
