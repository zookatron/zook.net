---
name: WordPress
level: Expert
image: /images/skills/wordpress.svg
---

I have been developing and maintaining complex WordPress websites on a daily basis for over a decade. I have extensive knowledge of the WordPress API and its plugin and theme ecosystem.
