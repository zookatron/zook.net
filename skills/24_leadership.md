---
name: Engineering Leadership
level: Experienced
image: /images/skills/leadership.svg
---

As CTO of Fahrenheit Marketing, I lead a tight-knit team of 5-10 developers and continually improved the company's development processes for four years.
