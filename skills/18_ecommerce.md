---
name: BigCommerce/Shopify
level: Expert
image: /images/skills/ecommerce.svg
---

I have been developing and maintaining complex BigCommerce/Shopify integrations on a daily basis for approximately four years. I have extensive knowledge of the BigCommerce and Shopify APIs and eCommerce web development in general.
