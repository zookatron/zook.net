---
name: TypeScript/JavaScript
level: Expert
image: /images/skills/typescript.svg
---

I have been developing and maintaining complex TypeScript applications on a daily basis for over 7 years. I have been working with JavaScript in general for over a decade.
