---
name: Technical Writing
level: Experienced
image: /images/skills/technical_writing.svg
---

As CTO of Fahrenheit Marketing, I communicated with our clients and my team mostly in writing since we were a remote-first company. I have extensive experience with documenting complex technical projects with precision and accuracy.
