---
name: Progressive Web Apps
level: Experienced
image: /images/skills/progressive_web_apps.svg
---

I'm highly passionate about developing for the modern web platform. I have extensive professional experience with modern progressive web application development.
