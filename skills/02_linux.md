---
name: Linux System Administration
level: Experienced
image: /images/skills/linux.svg
---

I have worked exclusively on Linux during my decade-plus of work experience. I have managed dozens of complex server configurations for many years.
