---
name: Non-violent Communication
level: Experienced
image: /images/skills/communication.svg
---

As CTO of Fahrenheit Marketing, I lead a tight-knit team of 5-10 developers for four years. I learned many valuable communication skills from the experience.
