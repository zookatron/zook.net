---
name: Continuous Integration
level: Expert
image: /images/skills/continuous_integration.svg
---

As CTO of Fahrenheit Marketing, I helped revolutionize our company's development processes by implementing, deploying, and maintaining GitLab continuous integration across all our projects.
