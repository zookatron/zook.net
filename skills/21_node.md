---
name: Node.js/Deno
level: Experienced
image: /images/skills/node.svg
---

I have extensive professional experience developing web application backends and developer tooling using Node.js and Deno. I prefer Deno due to its simplicity and elegance, but I recognize the pragmatic value of the Node.js ecosystem.
