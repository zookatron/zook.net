---
name: Python/Ruby
level: Hobbyist
image: /images/skills/python.svg
---

I have significant experience using Python and Ruby for various personal projects and scripts. I have a good understanding of the syntax and standard development processes for these languages.
