---
name: SQL DB Administration
level: Experienced
image: /images/skills/sql.svg
---

I have been managing numerous MariaDB and PostgreSQL servers for many years. I have extensive professional experience implementing and optimizing complex SQL queries.
