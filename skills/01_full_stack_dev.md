---
name: Full-stack Web Development
level: Expert
image: /images/skills/full_stack_dev.svg
---

I have worked on a diverse range of projects spanning the backend and frontend throughout my decade-plus of work experience.
