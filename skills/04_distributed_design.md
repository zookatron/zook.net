---
name: Distributed System Design
level: Experienced
image: /images/skills/distributed_design.svg
---

I have significant experience designing and implementing highly available and highly scalable web applications.
