---
name: REST API Design
level: Experienced
image: /images/skills/rest.svg
---

I have independently designed and implemented dozens of robust REST API interfaces in my decade-plus of work experience.
