---
name: Laravel
level: Expert
image: /images/skills/laravel.svg
---

I have been developing and maintaining complex Laravel web applications on a daily basis for approximately five years. I'm familiar with a variety of similar MVC frameworks in such as NestJS, Ruby on Rails, and Django.
