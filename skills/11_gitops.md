---
name: GitOps
level: Expert
image: /images/skills/gitops.svg
---

I have extensive professional experience managing large Git repositories, creating complex Git command line integrations, and working with Git-driven development processes.
