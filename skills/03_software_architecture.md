---
name: Software Architecture
level: Experienced
image: /images/skills/software_architecture.svg
---

I have independently designed and developed a wide range of web applications for many different industries. I thoroughly understand how to properly incorporate project requirements into an optimal architecture for the software solution.
